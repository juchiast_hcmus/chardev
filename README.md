# Hướng dẫn sử dụng

Đầu tiên, cần cài đặt các chương trình cần thiết cho việc dịch module.

Sau đó chạy lệnh `make` để dịch module.

Sau đó load module bằng lệnh `insmod randnum.ko`, chạy bằng quyền root.

Sau khi load xong, sẽ xuất hiện một character device ở đường dẫn `/dev/randnum`.

Để test device này, dùng một chương trình nào đó để mở và đọc từ device, ví dụ:
```
cat /dev/randnum | base64
```

Dùng lệnh `make clean` để  xóa các file đã dịch.