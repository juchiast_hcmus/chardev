#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/random.h>
#include <linux/mutex.h>

#define MIN(a, b) (a < b? a : b)

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Do Duy");
MODULE_DESCRIPTION("Random");
MODULE_VERSION("1");

static ssize_t randnum_read(struct file *, char *, size_t, loff_t *);

static struct file_operations fops = {
    .read = randnum_read,
};

#define NAME "randnum"
static int major_number;
static struct class *class;
static struct device *device;

DEFINE_MUTEX(mutex);
static int rand_seed;

static int xorshift32(int seed) {
    unsigned int x = seed;
    x ^= x << 13;
    x ^= x >> 17;
    x ^= x << 5;
    return x;
}

static int get_seed(void) {
    int seed;
    mutex_lock(&mutex);
    seed = rand_seed;
    rand_seed = xorshift32(seed);
    mutex_unlock(&mutex);
    return seed;
}

static void set_seed(int seed) {
    mutex_lock(&mutex);
    rand_seed = seed;
    mutex_unlock(&mutex);
}

static int fill_buffer(int *buffer, size_t size, int seed) {
    while (size > 0) {
        size--;
        buffer[size] = xorshift32(seed);
        seed = buffer[size];
    }
    return seed;
}

ssize_t randnum_read(struct file *file, char *to, size_t size, loff_t * offset) {
    int buffer[64], seed;
    size_t copy_size, failed;

    seed = get_seed();
    seed = fill_buffer(buffer, 64, seed);
    set_seed(seed);
    copy_size = MIN(size, sizeof(buffer));
    failed = copy_to_user(to, buffer, copy_size);
    if (failed > 0) {
        printk(KERN_ALERT "randnum: failed to copy %ld bytes\n", failed);
    }
    return copy_size - failed;
}

static char *randnum_setmode(struct device *dev, umode_t *mode) {
    if (mode) {
        *mode = 0444;
    }
    return NULL;
}

static int __init randnum_init(void) {
    int seed;

    major_number = register_chrdev(0, NAME, &fops);
    if (major_number < 0) {
        printk(KERN_ALERT "randnum: register_chrdev failed\n");
        return major_number;
    }

    class = class_create(THIS_MODULE, NAME);
    if (IS_ERR(class)) {
        printk(KERN_ALERT "randnum: class_create failed\n");
        unregister_chrdev(major_number, NAME);
        return PTR_ERR(class);
    }
    class->devnode = randnum_setmode;

    device = device_create(class, NULL, MKDEV(major_number, 0), NULL, NAME);
    if (IS_ERR(device)) {
        printk(KERN_ALERT "randnum: device_create failed\n");
        class_destroy(class);
        unregister_chrdev(major_number, NAME);
        return PTR_ERR(device);
    }

    get_random_bytes(&seed, sizeof(seed));
    mutex_init(&mutex);
    set_seed(seed);
    return 0;
}
module_init(randnum_init);

static void __exit randnum_exit(void) {
    mutex_destroy(&mutex);
    device_destroy(class, MKDEV(major_number, 0));
    class_destroy(class);
    unregister_chrdev(major_number, NAME);
}
module_exit(randnum_exit);